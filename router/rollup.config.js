import { nodeResolve } from '@rollup/plugin-node-resolve'

export default {
	input: './src/index.js',
	output: [{
		dir: 'dist/cjs',
		format: 'cjs',
	}, {
		dir: 'dist/es',
		format: 'es',
	}],
	external: [ /node_modules/ ],
	preserveModules: true,
	plugins: [ nodeResolve() ],
}
