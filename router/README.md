# Sruth Router

A routing library for Sruth.

```bash
npm i @sruth/core @sruth/router rxjs
```

## API

#### `navigate :: (path, options?) -> void`

Navigates to the specified path.

Accepts the following options:
- `replace` (optional, default `false`): whether to replace the current entry in the history or add a new one.
- `search` (optional, default `''`): search part of the url.

```javascript
navigate('/some/path', { search: '?query=true' })
```

#### `router :: (pages, factory) -> Observable`

Takes an array of pages and an observable factory and returns a function that you can pass to `subscribe`.

```javascript
import { subscribe } from '@sruth/core'
import { router } from '@sruth/router'

const pages = [
	{ path: '/' },
	{
		path: '/joke',
		run: () => {
			someEffect()
			return () => someCleanup()
		}
	},
	{ path: '/not-found', fallback: true }
]

const factory = router(routes, otherStateFactory)
const patch = state => vdom(state)

subscribe(patch, factory)
```

Each page can have the following properties:
- `fallback` (optional): set to true in order to fall back to a specific page if no match is found.
- `path`: the path to match the route.
- `run` (optional): a function to run when navigating to the page. It can optionally return a new function that will the executed when navigating away from the page.
- `view` (optional): a function that accepts state and location options (`params` and `query`); designed to be used to create a component to be rendered in the view.

