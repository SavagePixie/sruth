import tap from "tap"
import { Subject } from 'rxjs'

import { runEffects } from './effects.js'

tap.test('runEffects', t => {
	t.test('should call run function if present', t => {
		const params = { key: 'value' }
		const query = { key: 'value' }
		const subject = new Subject()
		const run = (p, s) => {
			t.equal(p.params, params)
			t.equal(p.query, query)
			t.equal(s, subject)
			t.end()
		}
		runEffects(subject)({}, { run, params, query, path: 'potato' })
	})

	t.test('should call cleanup function if present', t => {
		const params = { key: 'value' }
		const subject = new Subject()
		const cleanup = () => t.end()
		runEffects(subject)({ cleanup, params }, { path: 'potato' })
	})

	t.test('should not run effects if path is the same', t => {
		const cleanup = () => t.fail()
		const run = () => t.fail()
		const path = 'potato'
		const subject = new Subject()
		runEffects(subject)({ cleanup, path }, { path, run })
		t.end()
	})

	t.test('should return next', t => {
		const next = { path: '/' }
		const result = runEffects()({}, next)
		t.equal(result, next)
		t.end()
	})

	t.end()
})
