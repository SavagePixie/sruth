import { combineLatest, of } from "rxjs"

export const combineState =
	([ state, { params, path, query, route, view }, stream ]) => ({
		...state,
		location: {
			params,
			path,
			query,
			route,
			view: view ? view(state, { params, query }, stream) : null,
		},
	})

export const handlePageStream =
	data => {
		const [ state, { stream, ...page }] = data
		return stream
			? combineLatest([ of(state), of(page), stream(state) ])
			: of(data)
	}
