import { Observable, of } from 'rxjs'
import tap from 'tap'

import { combineState, handlePageStream } from './state.js'

tap.test('combineState', t => {
	const state = { count: 0, test: true }

	t.test('should merge location with rest of state', t => {
		const location = {
			cleanup: () => t.fail(),
			init: () => t.fail(),
			params: {},
			path: '/test',
			query: {},
			route: '/test',
		}
		const expected = {
			...state,
			location: {
				params: {},
				path: '/test',
				query: {},
				route: '/test',
				view: null,
			},
		}

		t.strictSame(combineState([ state, location ]), expected)
		t.end()
	})

	t.test('should execute view function if present', t => {
		const params = { test: 'test' }
		const query = { test: 'test' }
		const location = {
			params,
			query,
			view: (s, l) => {
				t.hasStrict(s, state)
				t.equal(l.params, params)
				t.equal(l.query, query)
				return { pass: true }
			},
		}

		t.hasStrict(
			combineState([ state, location ]),
			{ location: { view: { pass: true } } }
		)
		t.end()
	})

	t.test('should call view with stream if present', t => {
		const location = {
			view: (_state, _location, stream) => {
				t.equal(stream, 'stream')
				t.end()
			},
		}
		const stream = 'stream'
		combineState([{}, location, stream ])
	})

	t.end()
})

tap.test('handlePageStream', t => {
	const state = { count: 10 }
	const location = { params: { p: 'p' } }

	t.test('should return an observable', t => {
		const result = handlePageStream([ state, location ])
		t.ok(result instanceof Observable)
		t.end()
	})

	t.test('should return input if there is no stream', t => {
		const input = [ state, location ]
		handlePageStream(input)
			.subscribe(result => {
				t.equal(result, input)
				t.end()
			})
	})

	t.test('should call stream if present', t => {
		const stream = () => of('stream')
		handlePageStream([ state, { ...location, stream }])
			.subscribe(([ , , stream ]) => {
				t.equal(stream, 'stream')
				t.end()
			})
	})

	t.test('should pass state to stream', t => {
		const stream = state => of(state)
		handlePageStream([ state, { ...location, stream }])
			.subscribe(([ , , stream ]) => {
				t.same(stream, { count: 10 })
				t.end()
			})
	})

	t.end()
})
