const extract = location => {
	const search = new URLSearchParams(location.search)
	return Object.fromEntries(search.entries())
}

export const query = { extract }
