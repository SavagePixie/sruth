import { param, part, path } from "./path.js"
import { query } from "./query.js"

export const canHandle = location => route => {
	const parts = path.split(route, location)
	return part.match(...parts)
}

export const findPage = (pages, fallback = null) => location => {
	const found = pages.find(canHandle(location))
	const result = found ?? fallback ?? location
	return {
		...result,
		params: param.extract(found, location),
		path: found || !fallback ? location.path : fallback.path,
		query: query.extract(location),
		replace: location.replace,
		route: result.path,
		search: found ? location.search : '',
	}
}
