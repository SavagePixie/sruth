import tap from 'tap'

import { query } from './query.js'

tap.test('query.extract', t => {
	t.test('should return empty object if no search', t => {
		t.strictSame(query.extract({}), {})
		t.strictSame(query.extract({ search: '' }), {})
		t.strictSame(query.extract({ search: '?' }), {})
		t.end()
	})

	t.test('should build object with query arguments', t => {
		const location = { search: '?arg1=value1&arg2=value2&arg_number_three=value3' }
		const expected = {
			arg1: 'value1',
			arg2: 'value2',
			arg_number_three: 'value3',
		}

		t.strictSame(query.extract(location), expected)
		t.end()
	})

	t.end()
})
