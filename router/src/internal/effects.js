export const runEffects = stream => (prev, next) => {
	if(prev.path !== next.path) {
		prev.cleanup?.()
		const cleanup = next.run?.(
			{ params: next.params, query: next.query },
			stream
		)

		next.cleanup = typeof cleanup === 'function'
			? cleanup
			: undefined
	}
	return next
}

export const updateHistory = ({ path, replace = false, search }) => {
	const location = `${path}${search}`
	return replace
		? window.history.replaceState({ location }, '', location)
		: window.history.pushState({ location }, '', location)
}
