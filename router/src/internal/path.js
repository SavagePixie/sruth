const extractParams = (route, location) => {
	if (!route) return {}

	const [ locationParts, routeParts ] = splitPaths(location, route)
	return routeParts
		.reduce(
			(acc, current, idx) => isParam(current)
				? Object.assign(acc, { [current.slice(1)]: locationParts[idx] })
				: acc,
			{}
		)
}

const isParam = str => str.startsWith(':')

const partsMatch = (route, location) =>
	route.length === location.length
	&& route.every((p, idx) => isParam(p) || p === location[idx])

const splitPaths = (a, b) => {
	const splitA = trimPath(a.path).split('/')
	const splitB = trimPath(b.path).split('/')
	return [ splitA, splitB ]
}

const trimPath = str => {
	const start = str.startsWith('/')
		? 1
		: 0
	const end = str.endsWith('/')
		? -1
		: undefined
	return str.slice(start, end)
}

export const part = { match: partsMatch }

export const path = {
	split: splitPaths,
	trim: trimPath,
}

export const param = {
	extract: extractParams,
	is: isParam,
}
