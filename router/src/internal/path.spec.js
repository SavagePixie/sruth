import tap from 'tap'

import { param, part, path } from './path.js'

tap.test('param.extract', t => {
	t.test('should return empty object if no route is found', t => {
		t.strictSame(param.extract(undefined, { path: '/' }), {})
		t.end()
	})

	t.test('should return empty object if no params in route', t => {
		const route = { path: '/test/test/test' }

		t.strictSame(param.extract(route, route), {})
		t.end()
	})

	t.test('should build an object with all params in route', t => {
		const route = { path: '/test/:param1/test/:param2/test/:param3' }
		const location = { path: '/test/value1/test/value2/test/value3' }
		const expected = {
			param1: 'value1',
			param2: 'value2',
			param3: 'value3',
		}

		t.strictSame(param.extract(route, location), expected)
		t.end()
	})

	t.end()
})

tap.test('param.is', t => {
	t.test('should return true if it starts with :', t => {
		t.equal(param.is(':param'), true)
		t.end()
	})

	t.test('should return false if it starts with something else', t => {
		t.equal(param.is('param'), false)
		t.equal(param.is('!param'), false)
		t.equal(param.is(';param'), false)
		t.end()
	})

	t.end()
})

tap.test('part.match', t => {
	t.test('should return true if all parts match', t => {
		const path = [ 'a', 'b', 'c', 'd', 'e' ]

		t.equal(part.match(path, path), true)
		t.end()
	})

	t.test('should match params on the route side', t => {
		const route = [ 'a', ':b', ':c', 'd' ]
		const location = [ 'a', 'test', 'potato', 'd' ]

		t.equal(part.match(route, location), true)
		t.end()
	})

	t.test('should return false if one part does not match', t => {
		const route = [ 'a', 'b', 'c' ]
		const location = [ 'a', 'fail', 'c' ]

		t.equal(part.match(route, location), false)
		t.end()
	})

	t.test('should return false if arrays are not the same length', t => {
		const route = [ 'a', 'b', 'c' ]
		const location = [ 'a', 'b', 'c', 'd' ]

		t.equal(part.match(route, location), false)
		t.end()
	})

	t.test('should not match params on the location side', t => {
		const route = [ 'a', 'test', 'c' ]
		const location = [ 'a', ':b', 'c' ]

		t.equal(part.match(route, location), false)
		t.end()
	})

	t.end()
})

tap.test('path.split', t => {
	t.test('should return a tuple of arrays', t => {
		const route = { path: '/a/:b/c' }
		const location = { path: '/one/two/three' }
		const expected = [
			[ 'a', ':b', 'c' ],
			[ 'one', 'two', 'three' ],
		]

		t.strictSame(path.split(route, location), expected)
		t.end()
	})

	t.end()
})

tap.test('path.trim', t => {
	t.test('should remove leadin /', t => {
		t.equal(path.trim('/test'), 'test')
		t.equal(path.trim('/'), '')
		t.end()
	})

	t.test('should remove trailing /', t => {
		t.equal(path.trim('test/'), 'test')
		t.end()
	})

	t.test('should remove both leadig and trailing /', t => {
		t.equal(path.trim('/test/'), 'test')
		t.end()
	})

	t.test('should ignore middle /', t => {
		t.equal(path.trim('test/test'), 'test/test')
		t.equal(path.trim('/test/test/'), 'test/test')
		t.end()
	})

	t.end()
})
