import tap from "tap"

import { canHandle, findPage } from "./page.js"

tap.test('canHandle', t => {
	t.test('should be curried', t => {
		t.equal(typeof canHandle({ path: '/' }), 'function')
		t.end()
	})

	t.test('should return true if route can handle location', t => {
		const route = { path: '/test/:param' }
		const location = { path: '/test/value' }

		t.equal(canHandle(location)(route), true)
		t.end()
	})

	t.end()
})

tap.test('findPage', t => {
	t.test('should be curried', t => {
		t.equal(typeof findPage([]), 'function')
		t.end()
	})

	t.test('should return mathing route if any', t => {
		const pages = [{ path: '/' }, { path: '/test' }, { path: '/test/test' }]
		const result = findPage(pages)({ path: '/test' })

		t.hasStrict(result, { path: '/test', route: '/test' })
		t.end()
	})

	t.test('should return fallback if no match', t => {
		const pages = [{ path: '/' }, { path: '/test' }]
		const result = findPage(pages, { path: '/test' })({ path: '/not-found' })

		t.hasStrict(result, { path: '/test', route: '/test' })
		t.end()
	})

	t.test('should return location if no match and no fallback', t => {
		const pages = [{ path: '/' }, { path: '/test' }]
		const result = findPage(pages)({ path: '/not-found' })

		t.hasStrict(result, { path: '/not-found', route: '/not-found' })
		t.end()
	})

	t.test('should return first matching page', t => {
		const pages = [{ path: '/test/:param' }, { path: '/test/test' }]
		const result = findPage(pages)({ path: '/test/test' })

		t.hasStrict(result, { path: '/test/test', route: '/test/:param' })
		t.end()
	})

	t.test('should return params and query arguments', t => {
		const pages = [{ path: '/test/:param/some/:otherParam' }]
		const location = {
			path: '/test/value/some/other-value',
			search: '?query=true&moreQuery=also-true',
		}
		const expected = {
			params: {
				param: 'value',
				otherParam: 'other-value',
			},
			path: '/test/value/some/other-value',
			query: {
				query: 'true',
				moreQuery: 'also-true',
			},
			route: '/test/:param/some/:otherParam',
		}
		const result = findPage(pages)(location)

		t.hasStrict(result, expected)
		t.end()
	})

	t.test('should keep replace', t => {
		const find = findPage([{ path: '/test' }])

		t.hasStrict(find({ path: '/test', replace: true }), { replace: true })
		t.hasStrict(find({ path: '/', replace: true }), { replace: true })
		t.hasStrict(find({ path: '/test', replace: false }), { replace: false })
		t.hasStrict(find({ path: '/', replace: false }), { replace: false })
		t.end()
	})

	t.end()
})
