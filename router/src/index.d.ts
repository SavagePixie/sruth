import type { Observable } from 'rxjs'

export namespace Router {
	type Params = Record<string, string>
	type Query = Record<string, string>

	interface Data<V = void> {
		location: {
			params: Params
			path: string
			query: Query
			route: string
			view: V | null
		}
	}

	interface LocationArgs {
		params: Params
		query: Query
	}

	interface LocationStream {
		path: string
		replace: boolean
		search: string
	}

	interface Page<S extends {} = {}, V = null, PS = void> {
		fallback?: boolean
		run?: RunFunction<S>
		path: string
		stream?: (state: S) => Observable<PS>
		view?: (state: S, location: Router.LocationArgs, stream: PS) => V | null
	}

	type RunFunction<S extends {}> = (location: LocationArgs, stream: Observable<S>) => void | (() => void)
}

export function navigate(
	path: string,
	options?: {
		replace?: boolean
		search?: string
	}
): void

export function router<S extends {} = {}, V = null>(
	pages: Router.Page<S, V>[],
	observableFactory: (stream: Observable<S>) => Observable<S>
): (stream: Observable<S>) =>
	Observable<S & Router.Data<V>>

export declare const stream: Observable<Router.LocationStream>

