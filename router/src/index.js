import {
	auditTime, combineLatest, map,
	scan, startWith, Subject, switchMap, tap
} from 'rxjs'

import { runEffects, updateHistory } from './internal/effects'
import { findPage } from './internal/page'
import { combineState, handlePageStream } from './internal/state'

const location = new Subject()

const navigate = (path, { replace = false, search = '' } = {}) =>
	location.next({ path, replace, search })

const router = (pages, observableFactory) => {
	const fallback = pages.find(page => page.fallback === true)
	return stream => {
		const { pathname: path, search } = window.location
		const _location = location
			.pipe(
				startWith({ path, replace: true, search }),
				map(findPage(pages, fallback)),
				scan(runEffects(stream), {}),
				map(({ cleanup: _, ...rest }) => rest),
				tap(updateHistory)
			)

		return combineLatest([ observableFactory(stream), _location ])
			.pipe(
				auditTime(50),
				switchMap(handlePageStream),
				map(combineState)
			)
	}
}

const stream = location.asObservable()

export { navigate, router, stream }
