# How to contribute to Sruth

If you find something that you'd like to help with, your contribution will be greatly appreciated.

1. Create an issue describing what needs to be done.
1. Clone the repo and create a new branch named after the issue (e.gr. for issue 25, `25-some-feature`).
1. Write your code following the same style as the codebase.
1. Create any tests necessary to ensure your code functions properly.
1. Ensure that your code passes tests and linting.
1. Make a merge request to `master` branch.
