module.exports = {
	parserOptions: {
		ecmaVersion: 2022,
		sourceType: 'module',
	},
	extends: [ 'pixie' ],
	rules: {
		indent: [ 'error', 'tab' ],
		'max-len': [ 'error', {
			code: 80,
			ignoreComments: true,
			ignoreTemplateLiterals: true,
			ignoreRegExpLiterals: true,
			tabWidth: 0, // Ignores tabs when calculating line width
		}],
		'newline-per-chained-call': [ 'error' ],
		'no-multiple-empty-lines': [ 'error', { max: 1, maxBOF: 0, maxEOF: 1 }],
		'object-curly-newline': [ 'error', { multiline: true }],
		'object-property-newline': [ 'error', { allowAllPropertiesOnSameLine: true }],
	},
}
