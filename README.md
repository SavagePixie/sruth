# Sruth

Sruth (/sdruh/, "stream" in Gaelic) is a library for reactive state management using observables.

## Packages

Sruth is comprised of the following packages:

| Package | Description |
|---------|-------------|
| [`@sruth/core`](./core/README.md) | Main functionality |
| [`@sruth/router`](./router/README.md) | Routing |

## License

All packages are licensed under the [MIT License](./LICENSE).
