import { Router, navigate, router } from '@sruth/router'
import { Changer, change, subscribe } from '@sruth/core'
import { VNode, eventListenersModule, h, init, propsModule } from 'snabbdom'
import {
	Observable, Subject,
	combineLatest, interval,
	map, merge, startWith,
	switchMap,
	takeUntil
} from 'rxjs'

type Component<S> = Router.ViewComponent<State<S>, VNode>

interface CounterState {
	counter: number
}

interface DateState {
	date: Date
}

interface State<V = any> {
	page: string
	view: V
}

const counter = (() => {
	const countSubject = new Subject<number>()
	const setSubject = new Subject<number>()

	const decrease = () => countSubject.next(-1)
	const increase = () => countSubject.next(1)
	const set = (value: number) => setSubject.next(value)

	const toStateSet = (counter: number): Changer<CounterState> => ({
		changes: [ state => ({ ...state, counter })]
	})

	const toStateUpdate = (value: number): Changer<CounterState> => ({
		changes: [
			state => ({
				...state,
				counter: state.counter + value
			})
		]
	})

	const stream = merge(
		countSubject.pipe(map(toStateUpdate)),
		setSubject.pipe(map(toStateSet))
	).pipe(change({ counter: 0 }), startWith({ counter: 0 }))

	return { decrease, increase, set, stream }
})()

const dater = (() => {
	const startSubject = new Subject<void>()
	const stopSubject = new Subject<void>()

	const start = () => startSubject.next()
	const stop = () => stopSubject.next()

	const stream = startSubject.pipe(
		switchMap(() =>
			interval(200).pipe(
				takeUntil(stopSubject),
				map(() => new Date()),
				startWith(new Date()),
			)
		),
		map(date => ({ date }))
	)

	return { start, stop, stream }
})()

const page = (() => {
	const subject = new Subject<string>()
	const set = (value: string) => subject.next(value)
	const stream = subject.pipe(startWith(''))

	return { set, stream }
})()

const stateFactory = (): Observable<State<CounterState | DateState>> => combineLatest({
	page: page.stream,
	view: merge(counter.stream, dater.stream)
})

const Counter: Component<CounterState> = ({ view }) =>
	h('div', [
		h('button', { on: { click: counter.decrease } }, 'Decrease'),
		h('span', view.counter),
		h('button', { on: { click: counter.increase } }, 'Increase'),
	])

const Dater: Component<DateState> = ({ view }) =>
	h('div', view.date.toUTCString())

const Link = (href: string, label: string) => {
	const handleClick = (event: MouseEvent) => {
		event.preventDefault()
		navigate(href)
	}
	return h('a', {
		props: { href },
		on: { click: handleClick }
	}, label)
}

const routes: Router.Page<State, VNode>[] = [{
	fallback: true,
	path: '/counter',
	run: () => {
		counter.set(0)
		page.set('Counter Page')
	},
	view: Counter
}, {
	path: '/counter/:init',
	run: ({ params }) => {
		counter.set(Number(params.init) || 0)
		page.set('Counter Page')
	},
	view: Counter
}, {
	path: '/dater',
	run: () => {
		dater.start(),
		page.set('Dater Page')
		return dater.stop
	},
	view: Dater
}]

const vnode = ({ location, page }: State & Router.Data<VNode>) => h('main', [
	h('h1', 'Typescript Example'),
	h('nav', [
		Link('/counter', 'Counter'),
		Link('/dater', 'Dater')
	]),
	h('h2', page),
	location.view
])

const patch = () => {
	const update = init([ eventListenersModule, propsModule ])
	let node: HTMLElement | VNode | null = document.getElementById('app')
	return (state: State & Router.Data<VNode>) => {
		if (!node) throw new Error('Missing app node')
		node = update(node, vnode(state))
	}
}

subscribe(patch(), router(routes, stateFactory))


