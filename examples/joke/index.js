import {
	catchError, interval, map, merge, of,
	startWith, Subject, switchMap, timer
} from 'rxjs'
import { fromFetch } from 'rxjs/fetch'
import {
	attributesModule, eventListenersModule, h,
	init, styleModule
} from "snabbdom"
import { change, subscribe } from '../../core'

const LOADERS = [
	'Let me fetch another joke for you',
	'I\'ll get you another one',
	'Just a moment',
	'Getting a joke',
]

const Status = {
	idle: 'idle',
	loading: 'loading',
}

// Helpers
const changeBgColour = ({ colour }) => {
	document.querySelector('body').style.backgroundColor = colour
}

const chooseLoader = () => LOADERS[Math.floor(Math.random() * LOADERS.length)]

const generateNumber = () =>
	Math.floor(Math.random() * 256)
		.toString('16')
		.padStart(2, '0')

const generateColour = () => {
	const colours = Array.from({ length: 3 }, generateNumber).join('')
	return `#${colours}`
}

// State
const initState = {
	colour: '#fff',
	joke: 'No joke yet',
	loader: '',
	status: Status.idle,
}

// State setters
const setStatus = status => () => ({ status })
const changeColour = () => ({ colour: generateColour() })
const changeLoader = () => ({ loader: chooseLoader() })

// Filters
const isNotLoading = state => state.status != Status.loading

// Observable creation helpers
const fetchJoke = state => fromFetch(
	'https://icanhazdadjoke.com/',
	{ headers: { accept: 'application/json' } }
).pipe(
	switchMap(res => res.json()),
	catchError(() => of({ joke: 'Something went wrong' })),
	map(data => ({ ...state, joke: data.joke }))
)

const load = () => ({
	changes: [
		[ setStatus(Status.loading), changeLoader ],
		[ () => timer(1_000), fetchJoke, setStatus(Status.idle), changeColour ],
	],
	filters: [ isNotLoading ],
})

// Observables
const button = new Subject()
const timed = interval(30_000)

const observableFactory = subject => {
	subject.subscribe(changeBgColour)
	return merge(button, timed)
		.pipe(
			map(load),
			startWith({ changes: [ () => initState ] }),
			change()
		)
}

// View
const vnode = state => h('main', [
	h('h1', 'Joke'),
	h('p', state.joke),
	h('div.flex', [
		h(
			'button.joke-button',
			{
				attrs: { disabled: state.status == Status.loading },
				on: { click: () => button.next() },
				style: { backgroundColor: state.colour },
			},
			'Get a joke'
		),
		state.status == Status.loading ? h('p.info', state.loader) : null,
	]),
])

const patch = () => {
	const update = init([ attributesModule, eventListenersModule, styleModule ])
	let node = document.getElementById('root')
	return state => {
		node = update(node, vnode(state))
	}
}

subscribe(patch(), observableFactory)
