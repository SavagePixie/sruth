import {
	interval, map, scan,
	startWith, switchMap,
	Subject, takeUntil
} from 'rxjs'

const makeCounter = (key, obs$) => obs$
	.pipe(
		startWith(0),
		scan(acc => acc + 1),
		map(value => ({ [key]: value }))
	)

export const manual = makeCounter('manual', new Subject())
export const stopTimed = new Subject()
export const startTimed = makeCounter(
	'timed',
	new Subject().pipe(switchMap(() => interval(2_500).pipe(takeUntil(stopTimed))))
)
