import { h } from "snabbdom"

export const TimedCounter = state =>
	h('section.counter', [
		h('h2', 'Timed Counter'),
		h('span', state.timed),
	])

