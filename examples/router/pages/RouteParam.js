import { h } from "snabbdom"

export const RouteParam = ({ location: { params, query } }) =>
	h('section.counter', [
		h('h2', 'Param Route'),
		h('span', `Your :value is ${params.value}`),
		h('section', [
			h('h3', 'Query arguments'),
			h(
				'ul',
				Object.entries(query)
					.map(([ key, value ]) => h('li', `${key}: ${value}`))
			),
		]),
	])

