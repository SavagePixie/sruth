import { h } from "snabbdom"

import { manual } from "../sources"

export const ManualCounter = state =>
	h('section.counter', [
		h('h2', 'Manual Counter'),
		h('span', state.manual),
		h('button', { on: { click: () => manual.next() } }, 'Increase'),
	])

