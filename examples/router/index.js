import { merge as reducer, subscribe } from '@sruth/core'
import { navigate, router } from '../../router/src'
import { merge } from 'rxjs'
import { eventListenersModule, h, init, propsModule } from 'snabbdom'

import { ManualCounter } from './pages/ManualCounter'
import { RouteParam } from './pages/RouteParam'
import { TimedCounter } from './pages/TimedCounter'
import { manual, startTimed, stopTimed } from './sources'

const handleClick = fun => event => {
	event.preventDefault()
	fun()
}

// State
const factory = () => merge(manual, startTimed)
	.pipe(reducer())

const routes = [{
	path: '/',
	fallback: true,
}, {
	path: '/manual',
	view: ManualCounter,
}, {
	path: '/timed',
	run: () => {
		startTimed.next()
		return () => stopTimed.next()
	},
	view: TimedCounter,
}, {
	path: '/param/:value',
	view: RouteParam,
}]

// View
const NavLink = ({ href, title }) =>
	h('a',
		{
			props: { href },
			on: { click: handleClick(() => navigate(href)) },
		},
		title
	)

const vnode = state => h('main', [
	h('h1', 'Route Counters'),
	h('nav', [
		NavLink({ href: '/manual', title: 'Manual Counter' }),
		NavLink({ href: '/timed', title: 'Timed Counter' }),
	]),
	state.location.view,
	h('section', [
		h('h2', 'Current State'),
		h('pre', JSON.stringify(state, null, 2)),
	]),
])

const patch = () => {
	const update = init([ eventListenersModule, propsModule ])
	let node = document.getElementById('root')
	return state => {
		node = update(node, vnode(state))
	}
}

subscribe(patch(), router(routes, factory))

