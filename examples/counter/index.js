import { scan, startWith, Subject } from 'rxjs'
import { subscribe } from "../../core/src"

const count = new Subject()

window.onload = () => {
	document.body.addEventListener('click', event => {
		if (event.target.id == 'increase') count.next(1)
		if (event.target.id == 'decrease') count.next(-1)
	})
}

subscribe(
	state => document.getElementById('value').textContent = state,
	() => count.pipe(
		startWith(0),
		scan((a, b) => a + b)
	)
)
