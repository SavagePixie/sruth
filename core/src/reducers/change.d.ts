import type { Observable, OperatorFunction } from 'rxjs'

export type Change<S> = (state: S) => Partial<S> | Observable<Partial<S>>
export interface Changer<S> {
	changes: Change<S>[] | Change<S>[][]
	filters?: Filter<S>[]
}
export type Filter<S> = (state: S) => boolean

/**
 * Applies one of multiple lists of changes to the state if all the filters pass.
 */
export function change<S>(init?: S): OperatorFunction<Changer<S>, S>
