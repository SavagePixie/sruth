import type { OperatorFunction } from 'rxjs'

/**
 * Performs a shallow merge of the previous and current states.
 */
export function merge<S extends Record<string | number | symbol, unknown>>(init?: S): OperatorFunction<Partial<S>, S>
