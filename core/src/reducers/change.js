import { concat, concatMap, isObservable, map, mergeScan, of } from "rxjs"

const applyChange = (acc, change) =>
	acc.pipe(concatMap(state => {
		const next = change(state)
		return isObservable(next)
			? next.pipe(map(next => ({ ...state, ...next })))
			: of({ ...state, ...next })
	}))

const reduceChanges = state => changes =>
	changes.reduce(applyChange, of(state))

const change = init =>
	mergeScan((state, { changes = [], filters = [] } = {}) => {
		if (!filters.every(pred => pred(state))) return of(state)

		if (!changes.every(Array.isArray)) {
			return reduceChanges(state)(changes)
		}

		return concat(...changes.map(reduceChanges(state)))
	}, init)

export { change }
