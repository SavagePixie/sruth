import { scan } from "rxjs"

const merge = init => scan((acc, next) => ({ ...acc, ...next }), init)

export { merge }
