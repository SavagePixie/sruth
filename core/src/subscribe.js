import { distinctUntilChanged, Subject } from 'rxjs'

const subscribe = (patch, observableFactory) => {
	const subject = new Subject()
	const observer = subject.subscribe(patch)
	observableFactory(subject.asObservable())
		.pipe(distinctUntilChanged())
		.subscribe(subject)
	return observer
}

export { subscribe }
