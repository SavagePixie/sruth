import { from, of, Subject } from "rxjs"
import tap from "tap"

import { subscribe } from "./subscribe.js"

tap.test('should be a function', t => {
	t.equal(typeof subscribe, 'function')
	t.end()
})

tap.test('should throw on bad arguments', t => {
	t.test('first argument isn\'t a function', t => {
		t.throws(() => subscribe('potato'), TypeError)
		t.end()
	})

	t.test('second argument isn\'t a function', t => {
		t.throws(() => subscribe(() => null, 'potato'), TypeError)
		t.end()
	})

	t.end()
})

tap.test('state changes', t => {
	let counter = 0
	const patch = (t, resolve, expects) => state => {
		t.equal(state, expects[counter])
		if (counter == expects.length - 1) return resolve()
		counter++
	}

	t.beforeEach(() => {
		counter = 0
	})

	t.test('should call patch with state changes', t => {
		const states = [ 0, 1, 2 ]
		return new Promise(resolve => {
			subscribe(patch(t, resolve, states), () => from(states))
		})
	})

	t.test('should not call patch if new state is same as previous', t => {
		const states = [ 0, 1, 1, 1, 1, 2 ]
		const expects = [ 0, 1, 2 ]
		return new Promise(resolve => {
			subscribe(patch(t, resolve, expects), () => from(states))
		})
	})

	t.end()
})

tap.test(
	'should pass the subject as an observable as argument to factory',
	t => {
		const factory = stream => {
			t.ok(stream instanceof Object)
			t.notOk(stream instanceof Subject)
			t.end()
			return of(0)
		}
		subscribe(() => null, factory)
	})
