import type { Observable, Subscription } from 'rxjs'

/**
 * Initialises the state store and creates a subscription to it using the patch function.
 */
export function subscribe<S>(
	patch: (state: S) => void,
	observableFactory: (stream: Observable<S>) => Observable<S>
): Subscription
