# Sruth Core

A reactive state management library. It uses [RxJS](https://rxjs.dev/) to create observables, so you'll need it installed too.

```bash
npm i @sruth/core rxjs
```

## API

### Store

#### `subscribe :: (patch, factory) -> Subscription`

The main function that gets everything going. It creates an observable using the `factory` function and subscribes to it using the `patch` function.

Usually you'll want to pass a function that receives the current state and updates the DOM as the `patch`.

```javascript
import { interval } from 'rxjs'
import { subscribe } from '@sruth/core'

const patch = state => vdom(state)
const factory = () => interval(3_000)

subscribe(patch, factory)
```

`factory` receives an observable that you can use if you need to know the current state at some point in the observable stream.

```javascript
import { interval, map, withLatestFrom } from 'rxjs'
import { subscribe } from '@sruth/core'

const patch = state => vdom(state)
const factory = stream =>
	interval(3_000)
		.pipe(
			withLatestFrom(stream),
			map(([_, prev]) =>
				prev % 2 === 0
					? prev + 1
					: prev * 2
			)
		)

subscribe(patch, factory)
```

It can also be useful to run unrelated effects every time the state changes.

```javascript
subscribe(
	patch,
	stream => {
		stream.subscribe(console.log)
		return observable$
	}
)
```

### Reducers

Sruth exposes a couple of reducer functions to facilitate merging multiple observables into one state. They are there for your convenience, but you can also just create your own if they don't suit you.

#### `change :: initState -> Operator`

Returns an operator that expects an object describing state changes and, optionally, filters.

Each change is a function that receives the current state and returns a new state value or an observable. If multiple change functions are passed, they will all be merged and then a new state will be emitted.

```javascript
import { reducers } from '@sruth/core'

const initState = { count: 0 }
const increaseCount = value => state => ({ count: state.count + value })

const factory = () => interval(1_000)
	.pipe(map(value => ({ changes: [ increaseCount(value) ]})))
```

If filters are passed, it will first run all the filters against the current state. If any filter fails, it'll stop. If all pass, it'll apply the changes to state and emit a new state.

You can also emit multiple state changes from the same set of changes if you pass them in nested arrays. This is useful if you need multiple changes triggered depending on the same filter, for example, when making an http request and setting a loader.

```javascript
const load = () => ({
	changes: [
		[ setStatus('loading') ],
		[ fetchUser, setStatus('idle') ]
	],
	filters: [ isNotLoading ]
})
```

The above set of changes will trigger one state change first and set the status to `loading`, and another one later when it has finished fetching the user. But if the status is already `loading`, it will skip both changes.

For more examples on this reducer, you can check the [joke example](https://gitlab.com/SavagePixie/sruth/-/tree/master/examples/joke).

#### `merge :: initState -> Operator`

Returns an object that expects a partial state replacement and merges it with the previous state. It optionally takes an initial state.

```javascript
import { listen, reducers } from '@sruth/core'

const factory = () => {
	const name$ = listen('name-change')
		.pipe(map(name => ({ name })))
	const age$ = listen('age-change')
		.pipe(map(age => ({ age })))

	return merge(name$, age$)
		.pipe(
			startsWith({ name: 'empty', age: 0 }),
			reducers.merge()
		)
}
```
